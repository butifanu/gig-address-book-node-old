import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';

import App from './App';

/**
 * @name EntryPoint
 * @description This is the entry point container that we need to render our entire React app.
 *              The container is wrapped by the Provider component from the react-redux bindings and passes the store
 *              to our app.
 *              You can find more details about this process in the 'main.js' file from 'src'.
 */
export default function EntryPoint() {
    return (
        <Router>
            <Route path="/" component={App} />
        </Router>
    );
}
