import { minLength, onlyLetters, isEmail, isEmpty } from './validations';
import { camelToWords } from '../strings/camel-to-words';

/**
 * @name validate
 * @description Validation function that goes through all the validations and sets the state either if we have or not
 *              errors.
 * @param {string} field - the input field we want to validate
 * @param {string} value - the value we validate against
 * @param {function} setState - a reference to the setState function we use in React to change our state
 * @param {object} currentErrorState - the current error state, used to be concatenated with, to not lose errors
 * @example
 * validate('firstName', 'Doe', this.setState.bind(this), this.state.error);
 */
export const validate = (field, value, setState, currentErrorState) => {
    if (field !== 'country' && field !== 'email') {
        if (isEmpty(value)) {
            return setState(() => {
                return {
                    error: {
                        ...currentErrorState,
                        [`error${field}`]: {
                            message: `You cannot let ${camelToWords(field)} empty.`,
                            field,
                        },
                    },
                    hasNoErrors: false,
                };
            });
        }

        if (!minLength(value)) {
            return setState(() => {
                return {
                    error: {
                        ...currentErrorState,
                        [`error${field}`]: {
                            message: `You must enter a minimum of 2 characters for ${camelToWords(field)}.`,
                            field,
                        },
                    },
                    hasNoErrors: false,
                };
            });
        }

        if (!onlyLetters(value)) {
            return setState(() => {
                return {
                    error: {
                        ...currentErrorState,
                        [`error${field}`]: {
                            message: `You must enter only letters for ${camelToWords(field)}.`,
                            field,
                        },
                    },
                    hasNoErrors: false,
                };
            });
        }

        return setState(() => {
            return {
                error: {
                    ...currentErrorState,
                    [`error${field}`]: {
                        message: '',
                        field,
                    },
                },
                hasNoErrors: true,
            };
        });
    }

    if (field === 'email') {
        if (isEmpty(value)) {
            return setState(() => {
                return {
                    error: {
                        ...currentErrorState,
                        [`error${field}`]: {
                            message: 'You cannot let Email empty.',
                            field,
                        },
                    },
                    hasNoErrors: false,
                };
            });
        }

        if (!isEmail(value)) {
            return setState(() => {
                return {
                    error: {
                        ...currentErrorState,
                        [`error${field}`]: {
                            message: 'Your email is invalid, please enter a valid Email.',
                            field,
                        },
                    },
                    hasNoErrors: false,
                };
            });
        }

        return setState(() => {
            return {
                error: {
                    ...currentErrorState,
                    [`error${field}`]: {
                        message: '',
                        field,
                    },
                },
                hasNoErrors: true,
            };
        });
    }

    return setState(() => {
        return {
            error: {
                ...currentErrorState,
                [`error${field}`]: {
                    message: '',
                    field,
                },
            },
            hasNoErrors: true,
        };
    });
};
