/* mock contacts to be used when we start the app and if we don't want to have no entries (option can be configured
in src/configs/app-config) */
export const mockContacts = [
    {
        id: 1,
        firstName: 'Seba',
        lastName: 'Alin',
        email: 'alin@html5depot.com',
        country: {
            name: 'Romania',
            code: 'RO',
        },
    },
    {
        id: 2,
        firstName: 'Doe',
        lastName: 'John',
        email: 'john@doe.com',
        country: {
            name: 'United Kingdom',
            code: 'GB',
        },
    },
    {
        id: 3,
        firstName: 'Doe',
        lastName: 'Jane',
        email: 'jane@doe.com',
        country: {
            name: 'United States',
            code: 'US',
        },
    },
];
