const storage = window.localStorage || {};

/**
 * @name addToStorage
 * @description Add or edit values into localStorage at the specified key.
 * @param {string} key - the localStorage key we want to change
 * @param {*} value - the value we want to assign to it; can be anything
 */
export function addToStorage(key, value) {
    // check to see if the browser supports localStorage
    if (hasLocalStorage()) {
        if (getFromStorage(key) !== null && typeof value === 'object' && value !== null) {
            const currentStorage = getFromStorage(key);

            if (Object.prototype.toString.call(currentStorage) === '[object Array]') {
                const itemToReplace = currentStorage.find((item) => item.id === value.id);
                // edit contact
                if (itemToReplace && itemToReplace.id) {
                    const entryIndex = currentStorage.findIndex((item) => item.id === itemToReplace.id);
                    // replace the edited contact at the same place
                    currentStorage.splice(entryIndex, 1, value);
                    const updatedStorage = currentStorage;
                    return storage.setItem(key, JSON.stringify(updatedStorage));
                }

                // add new contact
                return storage.setItem(key, JSON.stringify([...value, ...currentStorage]));
            }

            // remove the entire storage and recreate it
            return removeAllStorage(key);
        }

        return storage.setItem(key, JSON.stringify(value));
    }

    return false;
}

/**
 * @name getFromStorage
 * @description Get the current value from localStorage at the specified key.
 * @param {string} key - the localStorage key we use in order to retrieve our localStorage collection
 */
export function getFromStorage(itemKey) {
    if (hasLocalStorage()) {
        return JSON.parse(storage.getItem(itemKey));
    }

    return false;
}

/**
 * @name removeAllStorage
 * @description Removes all from localStorage at the specified key.
 * @param {string} key - the localStorage key we use in order to remove everything from there
 */
export function removeAllStorage(key) {
    if (hasLocalStorage()) {
        return storage.removeItem(key);
    }

    return false;
}

/**
 * @name removeFromStorage
 * @description Removes an entry (based on its id) from localStorage at the specified key.
 *              This function is adjusted to fit our 'contacts' collection need.
 * @param {string} key - the localStorage key we use in order to remove an entry from that collection
 * @param {number} id - the id of the contact that should be removed
 */
export function removeFromStorage(itemKey, id) {
    if (hasLocalStorage()) {
        const currentStorage = getFromStorage(itemKey);
        const updatedStorage = currentStorage.filter((item) => item.id !== id);
        return storage.setItem(itemKey, JSON.stringify(updatedStorage));
    }

    return false;
}

/**
 * @name hasLocalStorage
 * @description Check to see if our browser supports localStorage. If not, it throws an error.
 */
function hasLocalStorage() {
    if (!storage) {
        throw new Error('Your browser does not support localStorage, please use another way to store things');
    }

    return true;
}
