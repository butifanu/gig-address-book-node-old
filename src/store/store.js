/**
 * General store that is meant to hold and manage the application state.
 */
import update from 'react-addons-update';

import { addToStorage, getFromStorage, removeFromStorage } from '../utils/storage/storage';

export const storeIdConst = {
    STORE_ID: 'appReducer',
};

const actionTypes = {
    GET_CURRENT_CONTACTS: 'get-current-contacts',
    ADD_MOCK_CONTACTS: 'add-mock-contacts',
    EDIT_CONTACT_ENTRY: 'edit-contact-entry',
    ADD_NEW_CONTACT_ENTRY: 'add-new-contact-entry',
    REMOVE_CONTACT_ENTRY: 'remove-contact-entry',
    SAVE_COUNTRIES: 'save-countries',
};

const defaultState = {
    contacts: [],
    countries: [],
};

/**
 * @name appReducer
 * @description This is the reducer of this app, which is the gateway for updating our store using actions and
 *              action creators.
 * @param {object} state - the state we are constantly updating by dispatching action creators
 * @param {object} action - this is the action object which contains the actionType (the action we need to access),
 *                          and other parameters passed when calling certain action creators, which we will use to
 *                          update our store state
 */
export function appReducer(state = defaultState, action = {}) {
    switch (action.type) {
        case (actionTypes.GET_CURRENT_CONTACTS): {
            return update(state, {
                contacts: { $set: action.currentStorage },
            });
        }
        case (actionTypes.ADD_MOCK_CONTACTS): {
            return update(state, {
                contacts: { $set: action.contacts },
            });
        }
        case (actionTypes.EDIT_CONTACT_ENTRY): {
            // edit a contact and replace it in the same position
            const newContact = action.contact;
            const currentState = state.contacts;
            const itemToReplace = currentState.find((item) => item.id === newContact.id);
            const indexToReplace = currentState.findIndex((item) => item.id === newContact.id);
            if (itemToReplace && itemToReplace.id) {
                return update(state, {
                    contacts: { $splice: [[indexToReplace, 1, newContact]] },
                });
            }

            // do nothing
            return state;
        }
        case (actionTypes.ADD_NEW_CONTACT_ENTRY): {
            const alreadyHasEntry = state.contacts.find((item) => item.id === action.contact.id);
            // if we already have the item, do nothing
            if (alreadyHasEntry) {
                return state;
            }

            // add the item at the beginning (newest), this is the default sorting
            return update(state, {
                contacts: { $unshift: [...action.contact] },
            });
        }
        case (actionTypes.REMOVE_CONTACT_ENTRY): {
            const updatedContacts = state.contacts.filter((contact) => contact.id !== action.id);
            return update(state, {
                contacts: { $set: updatedContacts },
            });
        }
        case (actionTypes.SAVE_COUNTRIES): {
            return update(state, {
                countries: { $push: action.countries },
            });
        }
        default: {
            return state;
        }
    }
}

/**
 * @name getCurrentContacts
 * @description Used to get the current contacts from localStorage.
 */
export function getCurrentContacts() {
    const currentStorage = getFromStorage('contacts');
    return {
        type: actionTypes.GET_CURRENT_CONTACTS,
        currentStorage,
    };
}

/**
 * @name addMockContacts
 * @description Used to get the current contacts from localStorage.
 *              Will be synchronised with localStorage.
 * @param {array} contacts - the mock data that will be added to the contacts list
 */
export function addMockContacts(contacts) {
    addToStorage('contacts', contacts);
    return {
        type: actionTypes.ADD_MOCK_CONTACTS,
        contacts,
    };
}

/**
 * @name editContactEntry
 * @description Edit a contact and update it in the store and also in the localStorage.
 * @param {object} contact - the contact object that will replace the old one
 */
export function editContactEntry(contact) {
    addToStorage('contacts', contact);
    return {
        type: actionTypes.EDIT_CONTACT_ENTRY,
        contact,
    };
}

/**
 * @name addNewContactEntry
 * @description Add a new contact in the store and also in the localStorage.
 * @param {object} contact - the contact object that will be added to the currently existing store of contacts
 */
export function addNewContactEntry(contact) {
    addToStorage('contacts', contact);
    return {
        type: actionTypes.ADD_NEW_CONTACT_ENTRY,
        contact,
    };
}

/**
 * @name removeContactEntry
 * @description Removes a contact from the store and also from the localStorage.
 * @param {number} id - the id of the contact that should be removed
 */
export function removeContactEntry(id) {
    removeFromStorage('contacts', id);
    return {
        type: actionTypes.REMOVE_CONTACT_ENTRY,
        id,
    };
}

/**
 * @name saveCountries
 * @description Save all the countries from the node country-list package, in order to be used from here and to not
 *              call the module methods every time we need the list.
 * @param {array} countries - an array with all the countries
 */
export function saveCountries(countries) {
    return {
        type: actionTypes.SAVE_COUNTRIES,
        countries,
    };
}
