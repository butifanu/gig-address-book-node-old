import { createStore, combineReducers } from 'redux';

/**
 * Inits redux and stores
 */
let reducers = {
    dummy: (state = {}) => {
        return state;
    },
};
let store;

/**
 * Get the current store
 * @returns {*} the store
 */
export function getStore() {
    return store;
}

/**
 * Gets initial reducers and creates the store
 * @param {Object} reducerList holds all reducers for init (now we have only one, but we can pass multiple)
 * @returns {*} Newly created store
 */
export function init(reducerList) {
    reducers = Object.assign({}, reducers, reducerList);
    store = createNewStore(reducers);

    return store;
}

/**
 * Combine all the reducers and add enhancers to the store (such as the Redux DevTools)
 * @param {Object} contains all the reducers from which we will create the store
 * @returns {*} Newly created store
 */
function createNewStore(reducers) {
    return createStore(
        combineReducers(reducers), // even though we have only one reducer now, we might have others too
        // Use chrome devtool extension instead of bundling into app
        window.devToolsExtension ? window.devToolsExtension() : f => f,
    );

}
