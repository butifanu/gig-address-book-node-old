var path = require('path');
var fs = require('fs');

var webpack = require('webpack');
var HtmlWebpackPlugin = require('html-webpack-plugin');

// default build config constants
var buildConfigDefault = require(`${__dirname}/webpack-build-config`);

var babelConfig = JSON.parse(fs.readFileSync(path.resolve(`${buildConfigDefault.ABS_PATH}/.babelrc`), 'utf-8')); // eslint-disable-line max-len
var jsLoaderConfig = JSON.stringify(babelConfig);
var jsLoaders = [`babel-loader?${jsLoaderConfig}`];

var buildOutputPath = buildConfigDefault.DEVELOPMENT ? buildConfigDefault.DEV_PATH : buildConfigDefault.DIST_PATH;
var enableSourceMap = buildConfigDefault.DEVELOPMENT ?
    buildConfigDefault.WEBPACK_ENABLE_SOURCEMAPS_DEV : buildConfigDefault.WEBPACK_ENABLE_SOURCEMAPS_BUILD;

// path used in module js loaders include property
var jsModuleIncludePath = buildConfigDefault.SRC_PATH;
// path used isn module style loaders include property
var styleModuleIncludePath = path.resolve(buildConfigDefault.SRC_PATH, 'assets/styles');


// loaders configs
var cssLoaderConfig = {
    importLoaders: 2,
    sourceMap: enableSourceMap,
};

var devOnlyPlugins = [
    new webpack.NamedModulesPlugin(),
];

// to be completed...
var prodOnlyPlugins = [];


module.exports = {
    target: 'web',
    context: buildConfigDefault.ABS_PATH,
    entry: path.resolve(buildConfigDefault.SRC_PATH, 'main.js'),
    output: {
        path: buildOutputPath,
        filename: path.join(buildConfigDefault.STATIC_RELATIVE_JS, '/[name].[chunkhash].js'),
        sourceMapFilename: path.join(buildConfigDefault.STATIC_RELATIVE_JS, '/[name].js.map'),
        chunkFilename: path.join(buildConfigDefault.STATIC_RELATIVE_JS, '/[name].[chunkhash].js'),
        publicPath: buildConfigDefault.WEBPACK_PUBLIC_PATH,
    },
    module: {
        rules: [
            {
                test: /\.js$/i,
                include: jsModuleIncludePath,
                use: jsLoaders,
            },
            {
                test: /\.(scss|css)/i,
                include: styleModuleIncludePath,
                use: [
                    { loader: 'style-loader' },
                    {
                        loader: 'css-loader',
                        options: cssLoaderConfig,
                    },
                    { loader: 'sass-loader' },
                ],
            },
            {
                test: /\.(woff|woff(2))$/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {}
                    }
                ]
            }
        ],
    },
    resolve: {
        extensions: ['.json', '.js', '.css', '.scss', '.woff', '.woff2'],
    },

    devtool: enableSourceMap ? 'cheap-module-source-map' : false,
    devServer: {
        contentBase: buildConfigDefault.DEV_PATH,
        // This is handy if you are using a html5 router.
        historyApiFallback: true,
        // Set this if you want to enable gzip compression for styles
        noInfo: true,
        compress: true,
        stats: { colors: true },
        port: 3000,
    },
    plugins: [
        // index.html
        new HtmlWebpackPlugin({
            template: path.join(buildConfigDefault.SRC_PATH, 'index.html'),
            filename: path.join(buildOutputPath, 'index.html'),
            chunksSortMode: 'dependency',
            minify: buildConfigDefault.DEVELOPMENT ? undefined : {
                removeComments: true,
                preserveLineBreaks: false,
                collapseWhitespace: true,
                minifyCSS: true,
            },

        }),

        new webpack.optimize.CommonsChunkPlugin({
            name: "commons",
            filename: path.join(buildConfigDefault.STATIC_RELATIVE_JS, 'commons.js'),
            minChunks: 2,
        }),

    ].concat(buildConfigDefault.DEVELOPMENT ? devOnlyPlugins : prodOnlyPlugins),
};