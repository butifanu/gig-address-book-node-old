# README #

## GiG Address Book

### What is this app about ###

* This is a minimalist address book based on **localStorage**, **React** for the view, **Redux** for synchronisation with localStorage and **Webpack** for bundling;
* If the localStorage is empty, the storage will be populated with some mock entries;
* You can edit, add and remove entries from the address book;
* You have proper validations for the inputs;
* You can also sort the entries by the **'Oldest (default)'**, the **'Newest'**, **'Alphabetically'** and by **'Country'**;
* The app also has a config file (found in **src/configs/app-config.js**), in where you can change app starting options, such as loading some default mock data if we have no contacts at all.

### NPM Commands ###

* **npm run start** - starts WebpackDevServer and fires up the app, available on port 3000;
* **npm run build** - uses Babel to compile compatible JS code;
* **npm run eslint** - checks out the JS and JSX code for lint issues, using the rules from .eslintrc.

### Web technologies used ###

* [Webpack](https://webpack.js.org/), with various loaders and plugins;
* [WebpackDevServer](https://webpack.js.org/configuration/dev-server/) - Express-based server for Webpack;
* [React](https://reactjs.org/) - the popular view library;
* [localStorage](https://developer.mozilla.org/en-US/docs/Web/API/Storage/LocalStorage) - persistent browser storage;
* [Redux](http://redux.js.org/) - serves as the store, synchronising from the localStorage;
* [Babel](https://babeljs.io/) - for ES6 features transpilation;
* [ESLint](https://eslint.org/) - code linting tool for JS and JSX;
* [SASS](http://sass-lang.com/) - the popular CSS preprocessor.