const path = require('path');

const ABS_PATH = path.resolve('./');
const SRC_PATH = path.join(ABS_PATH, 'src');
// static dirs to use in HtmlWebpackPlugin options
const STATIC_RELATIVE = 'static';
const STATIC_RELATIVE_JS = path.join(STATIC_RELATIVE, '/js');
const STATIC_RELATIVE_CSS = path.join(STATIC_RELATIVE, '/assets/styles');

module.exports = {
    DEVELOPMENT: false,
    // paths
    ABS_PATH: ABS_PATH,
    SRC_PATH: SRC_PATH,
    DIST_PATH: path.join(ABS_PATH, 'dist', 'prod'),
    DEV_PATH: path.join(ABS_PATH, 'dist', 'dev'),
    STATIC_RELATIVE_JS: STATIC_RELATIVE_JS,
    STATIC_RELATIVE_CSS: STATIC_RELATIVE_CSS,

    // webpack specific
    WEBPACK_ENABLE_SOURCEMAPS_DEV: true,
    WEBPACK_ENABLE_SOURCEMAPS_BUILD: false,

    WEBPACK_PUBLIC_PATH: '/',
};
